package lab4.java.frontend;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lab4.java.common.Task;

import javax.swing.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class UpsertTaskFrm extends JDialog {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField tfdName;

    @FXML
    private TextField tfdTags;

    @FXML
    private TextArea tfdDescription;


    @FXML
    private Button btnUpsert;

    @FXML
    private DatePicker dpDeadline;

    @FXML
    void initialize(MainFrm.actFrm act, Task task) {
        btnUpsert.setText(act == MainFrm.actFrm.INSERT ? "Добавить" : "Изменить");
        if (act == MainFrm.actFrm.UPDATE) {
            tfdName.setText(task.getName());
            tfdDescription.setText(task.getDescription());
            dpDeadline.setValue(task.getDeadline());
            tfdTags.setText(task.getTags().stream().reduce((x, y )-> x+" " + y).get());
        }
        btnDisable();
        tfdName.setOnKeyReleased(event -> btnDisable());
        btnUpsert.setOnAction(event -> {
            if (act == MainFrm.actFrm.INSERT)
                addTask();
            else
                updTask(task);
        });
    }

    private void btnDisable(){
        btnUpsert.setDisable(tfdName.getText().isEmpty());
    }

    private void updTask(Task task) {
        task.setName(tfdName.getText());
        task.setDescription(tfdDescription.getText());
        task.setDeadline(dpDeadline.getValue());
        task.setTags(getTags());
        MainFrm.taskDao.update(task);
        close();
    }

    private void addTask() {
        MainFrm.taskDao.insert(new Task(tfdName.getText().trim(), tfdDescription.getText().trim(), dpDeadline.getValue(), getTags()));
        setVisible(false);
        close();
    }

    private ArrayList <String> getTags() {
        return Arrays.stream(tfdTags.getText().split(" "))
                .map(String::trim).collect(Collectors.toCollection(ArrayList::new));
    }

    private void close() {
        // close the form
        btnUpsert.getScene().getWindow().hide();
    }

}
