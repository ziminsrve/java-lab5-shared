package lab4.java.frontend;

import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lab4.java.common.TaskDao;

public class ConnectionFrm {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField tfdHost;

    @FXML
    private TextField tfdPort;

    @FXML
    private Button btnConnect;

    @FXML
    private Label lblMess;

    @FXML
    void initialize() {
        btnConnect.setOnAction(event -> start(null));
        tfdHost.setOnKeyReleased(event -> enableBtnConnect());
        tfdPort.setOnKeyReleased(event -> enableBtnConnect());
    }

    private void enableBtnConnect(){
        btnConnect.setDisable(tfdHost.getText().trim().isEmpty() && tfdPort.getText().trim().isEmpty());
    }

    private void start(TaskDao td) {
        lblMess.setText("");
        Socket socket = null;
        try {
                socket = new Socket(tfdHost.getText(), Integer.parseInt(tfdPort.getText()));
        } catch (IOException e) {
            e.printStackTrace();
            lblMess.setText("Не удалось подключиться!");
            return;
        }
        Stage mainStage = new Stage();
        MainFrm.taskDao = td;
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/mainFrm.fxml"));
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainStage.setTitle("Задачник");
        mainStage.setScene(new Scene(root, 930, 400));
        try {
            loader. <MainFrm>getController().initialize(socket);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainStage.show();
        mainStage.setResizable(false);
    }
}
