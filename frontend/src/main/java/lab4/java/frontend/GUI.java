package lab4.java.frontend;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lab4.java.common.TaskDao;

import java.io.IOException;

public class GUI extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/connectionFrm.fxml"));
        primaryStage.setTitle("Подключение");
        primaryStage.setScene(new Scene(root, 258, 214));
        primaryStage.show();
        primaryStage.setResizable(false);
    }
}
