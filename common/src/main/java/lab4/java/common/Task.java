package lab4.java.common;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

public class Task extends Subtask implements Comparable<Task> {

    private LocalDate deadline = null;
    private ArrayList<String> tags = new ArrayList<>();
    private ArrayList<Subtask> subtasks = new ArrayList<>();

    public Task(String name, String description, LocalDate deadline, ArrayList <String> tags) {
        super(name, description);
        this.deadline = deadline;
        this.tags = tags;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList <String> tags) {
        this.tags = (ArrayList <String>) tags.stream().distinct().collect(Collectors.toList());
        this.tags.remove("");
    }

    public ArrayList<Subtask> getSubtasks() {
        return subtasks;
    }

    @Override
    public void setCompleted() {
        super.setCompleted();
        getSubtasks().forEach(Subtask::setCompleted);
    }

    @Override
    public String toString() {
        //ФП
        return  super.toString() + String.format("%s %s%s"
                ,Optional.ofNullable(deadline).isPresent() ? String.format(" |До %s|",
                        deadline.format(DateTimeFormatter.ofPattern("dd.MM.yyyy (E)"))) : " "
                ,tags.stream().filter(x -> !x.trim().isEmpty())
                        .map(x -> " #" + x).reduce((x, y) -> x + y).orElse("")
                ,(subtasks.size() != 0 ? subtasks.stream().map(x -> String.format("\n\t%s", x))
                        .reduce((u,v) -> u + v).orElse("") : ""));
    }

    public int compareTo(Task other) {
        return getName().compareTo(other.getName());
    }

    public static Comparator<Task> COMPARE_BY_DATE = Comparator.comparing(Task::getDeadline);

    public static Comparator<Task> COMPARE_BY_DATE_DESC = (one, other) -> other.getDeadline().compareTo(one.getDeadline());
}
