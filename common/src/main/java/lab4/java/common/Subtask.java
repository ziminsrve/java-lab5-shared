package lab4.java.common;

import java.io.Serializable;

public class Subtask implements Serializable {
    private String name = "";
    private String description = "";
    private boolean completed = false;

    private int id = 0;

    Subtask(){}
    public Subtask(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted() {
        this.completed = true;
    }

    public String toString() {
        return id + ". " + name + (!description.isEmpty() ? String.format(" (%s) ", description): "");
    }
}
